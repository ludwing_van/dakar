drop TABLE diaindependencia;
drop TABLE diatrabajo;
drop TABLE ramadan;


CREATE TABLE diaindependencia (
        id bigserial,
        uid bigint,
        fechahora timestamp without time zone,
        siteid integer,
        lat double precision,
        lon double precision
);

CREATE TABLE diatrabajo (
        id bigserial,
        uid bigint,
        fechahora timestamp without time zone,
        siteid integer,
        lat double precision,
        lon double precision
);


CREATE TABLE ramadan (
        id bigserial,
        uid bigint,
        fechahora timestamp without time zone,
        siteid integer,
        lat double precision,
        lon double precision
);

insert into diaindependencia(uid,fechahora,siteid,lat,lon) 
	select id,fechahora,siteid,lat,lon from dakarset02 
 	where extract(month from fechahora)=4 and 
 	extract(day from fechahora)=4   
 	order by 1,2;


insert into diatrabajo(uid,fechahora,siteid,lat,lon) 
	select id,fechahora,siteid,lat,lon from dakarset02 
 	where extract(month from fechahora)=5 and 
 	extract(day from fechahora)=1   
 	order by 1,2;

insert into ramadan(uid,fechahora,siteid,lat,lon) 
	select id,fechahora,siteid,lat,lon from dakarset02 
 	where extract(month from fechahora)=7 and 
 	extract(day from fechahora)=9   
 	order by 1,2;


DELETE FROM diatrabajo
WHERE id IN (SELECT id
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY uid,fechahora ORDER BY id) AS rnum
                     FROM diatrabajo) t
              WHERE t.rnum > 1);

DELETE FROM ramadan
WHERE id IN (SELECT id
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY uid,fechahora ORDER BY id) AS rnum
                     FROM ramadan) t
              WHERE t.rnum > 1);

DELETE FROM diaindependencia
WHERE id IN (SELECT id
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY uid,fechahora ORDER BY id) AS rnum
                     FROM diaindependencia) t
              WHERE t.rnum > 1);
