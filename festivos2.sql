-- insert into annuevo(uid,fechahora,siteid,lat,lon) 
-- 	select id,fechahora,siteid,lat,lon from dakarset02 
--  	where fechahora::date = '2013-12-31'  
--  	order by 1,2;


--  	DELETE FROM annuevo
-- WHERE id IN (SELECT id
--               FROM (SELECT id,
--                              ROW_NUMBER() OVER (partition BY uid,fechahora ORDER BY id) AS rnum
--                      FROM annuevo) t
--               WHERE t.rnum > 1);

CREATE TABLE abril (
        id bigserial,
        uid bigint,
        fechahora timestamp without time zone,
        siteid integer,
        lat double precision,
        lon double precision
);

insert into abril(uid,fechahora,siteid,lat,lon) select id,fechahora,siteid,lat,lon from dakarset02 
  where extract(month from fechahora)=4 order by 1,2 ;


DELETE FROM abril
WHERE id IN (SELECT id
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY uid,fechahora ORDER BY id) AS rnum
                     FROM abril) t
              WHERE t.rnum > 1);

DELETE from abril
where uid in (
select uid from (
select uid, count(*) as llamadas from abril  group by 1) as A where A.llamadas < 10
);