from django.db import models
from django import forms
import psycopg2
from django.forms.extras.widgets import *
from dakar01.consultas import *
import csv
import os


# Create your models here.

conn_string = "host='localhost' dbname='senegal' user='chris' password=''"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()


class Heatpoints(models.Model):
    """docstring for Heatpoints"""
    def __init__(self):
        pass

    def listadepuntos(self):
        listafinal = []
        workpath = os.path.dirname(os.path.abspath(__file__)) #Returns the Path your .py file is in
        # c = open(os.path.join(workpath, 'file.csv'), 'rb')
        with open(os.path.join(workpath, 'heattotal.csv'), 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                # rowaux = [float(row[0]), float(row[1])] puntos totales
                rowaux = [float(row[0]), float(row[1])]
                listafinal.append(rowaux)
        print listafinal
        return listafinal

class ODuser(models.Model):
    """docstring for Antenas"""
    def __init__(self, consulta):
        self.consulta = consulta

    def getpoints(self):
        cur.execute(self.consulta)
        listcoor = cur.fetchall()
        strcoord = []
        n = len(listcoor)
        for x in xrange(0, n):
            arraux = [listcoor[x][0], listcoor[x][1]]
            strcoord.insert(x, arraux)
        return strcoord