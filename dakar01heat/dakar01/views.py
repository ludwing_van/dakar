from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from dakar01.models import *
from dakar01.consultas import *
import json


def index(request):
    # OBTENER ARRAY DE PUNTOS
    # objt = Heatpoints()
    # points = objt.listadepuntos()
    # context = {'points': points}
    # return render(request, 'indexh.html', context)
    obj1 = ODuser(origenes())
    obj2 = ODuser(destinos())
    orig = obj1.getpoints()
    dest = obj2.getpoints()
    context = {'orig': orig, 'dest': dest}
    return render(request, 'index.html', context)
