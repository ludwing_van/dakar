from django.conf.urls import include, url
from django.contrib import admin
from dakar01.views import index

urlpatterns = [
    # Examples:
    # url(r'^$', 'cancer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name='index'),
    # url(r'^get_dest/$', 'dakar01.views.get_dest', name='get_dest'),
]