# Algoritmo para determinar viajes por usuario, multiproceso.
# version TOTALMENTE CORREGIDA 29 septiembre 2015
# Revision 3 octubre 2016
# la tabla debe de tener valores no repetidos y ordenada por fechahora
#  //////ORDEN DE LA TABLA/////
# id|fechahora|siteid|lat| lon
import psycopg2
import psycopg2.pool
import geopy
import pdb
import multiprocessing
import math
from geopy.distance import vincenty

conn_string = "host='localhost' dbname='senegal' user='chris' password=''"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()


class multithre(multiprocessing.Process):
    def __init__(self, target, args):
        self.target = target
        self.args = args
        multiprocessing.Process.__init__(self)

    def run(self):
        self.target(*self.args)

# EXTRAE TODOS LOS USUARIOS DE LA BASE DE DATOS


def idslist(n):
    print "entro"
    # DEBE DE SER LATITUD,LONGITUD LAS COORDENADAS
    cur.execute("SELECT DISTINCT(uid) from noviembre;")
    # ids lista de ids unicos
    ids = cur.fetchall()
    print ids, "\n"
    nu = len(ids)
    div = int(math.ceil(nu/n))
    tids = []
    val = nu % n
    # Si la lista esta dividida en partes desiugales
    for x in range(0, n):
        if val != 0 and x == (n-1):
            tids.append(ids[x*div:])
        else:
            tids.append(ids[x*div:(x+1)*div])
    if val != 0:
        aux = tids[n-1][:div]  # valores normalizados ultimo espacio
        aux2 = tids[n-1][div:]  # sobrantes
        tids[n-1] = aux
        for x in range(0, val):
            tids[x].append(aux2[x])
    return tids


def createconnection():
    conn_string1 = "host='localhost' dbname='senegal' user='chris' password=''"
    conna = psycopg2.connect(conn_string1)
    return conna


def closeconnection(connection, cursor):
    cursor.close()
    connection.close()


# INSERTO EN UNA SOLA TABLA NO EN DOS PARA EVITAR CONFUSIONES
def insertondb(connection, cursor, site1, site2):
    #  uid|forigen |oid|olat | olon |fdestino | did |dlat |dlon
    cursor.execute("INSERT INTO matrizodaux(uid,forigen, oid, olat, olon, fdestino, did, dlat, dlon) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                   (site1[0], site1[1], site1[2], site1[3], site1[4],
                    site2[1], site2[2], site2[3], site2[4]))
    connection.commit()

# REGRESA LOS CDRs DEL USUARIO n


def trip(cursor, i):
    cursor.execute("SELECT uid,fechahora,siteid,lat,lon FROM noviembre WHERE uid = %s ", (i))
    totaltrip = cursor.fetchall()
    return totaltrip

# DIVIDE LOS USUARIOS DE MANERA INDIVIDUAL


def tripi(connection, cursor, listids):
    for i in listids:
        # analizar trip por trip
        print 'analysing {}'.format(i)
        totaltrip = trip(cursor, i)
        analyzetrip(connection, cursor, totaltrip)


def setdest(connection, cursor, oraux, site):
    # excepcion por si origen y destino en el mismo radio
    odaux = [oraux.pop(), site]
    # print "trip: ", odaux[0][1:3], odaux[1][1:3]
    insertondb(connection, cursor, odaux[0], odaux[1])
    orig = True
    return orig


def timesmplace(connection, cursor, oraux, site1, site2, est):
    deltat = 1200  # segundos 20min
    deltau = 10800  # un viaje no puede durar mas de un 3 horas
    timedelta = site2[1] - site1[1]
    deltai = timedelta.total_seconds()
    timedelta = site1[1] - oraux[0][1]
    deltaj = timedelta.total_seconds()

    # print "Tmin: ", ((deltai/60)/60), " Tmax: ", ((deltaj/60)/60)
    if deltai >= deltat and deltaj < deltau:
        orig = setdest(connection, cursor, oraux, site1)
    elif est and deltaj < deltau:
        orig = setdest(connection, cursor, oraux, site1)
    elif deltaj > deltau:
        # NUEVO AGREGADO
        orig = True
    else:
        # print "no se completo viaje o sigue viaje"
        orig = False
    return orig

# ALGORITMO PRINCIPAL ANALIZA EL VIAJE DEL USUARIO, EXTRAE TRIPS


def analyzetrip(connection, cursor, trip):
    oraux = []
    rdistance = 3000

    # true para agregar a origen
    nran = len(trip)
    # el primero sera origen
    orig = True
    for i in range(nran-1):
        if orig:
            # AGREGA ORIGEN SI NUEVO VIAJE
            # print "nuevo origen: ", trip[i][1:3]
            oraux.append(trip[i])
            orig = False
        # print trip[i][2]
        #  //////ORDEN DE LA TABLA/////
        # id|fechahora|siteid|lat| lon
        # SI NO ESTA EN EL MISMO LUGAR
        if trip[i][2] != trip[i + 1][2]:
            # print trip[i][2], "!=", trip[i + 1][2]
            site1 = trip[i][3:]
            site2 = trip[i+1][3:]
            # MIDE LA DISTANCIA QUE DEBERA SER MAYOR A 3000m
            deltadistance = vincenty(site1, site2).meters
            # TRUE EL USUARIO SE MOVIO SIGUIENTE PUNTO ANALIZAR
            if deltadistance > rdistance:
                # print "se movio mas 3km"
                pass
            else:
                # EL USUARIO SE QUEDO ESTATICO
                # VERIFICA SI LA DISTANCIA DEL ORIGEN AL PUNTO ACTUAL ES MAYOR
                # A 3 KM
                # print "quedo en el mismo radio"
                if vincenty(oraux[0][3:], site1).meters > rdistance:
                    # SE MANDA A AGREGAR AL VIAJE
                    # print "esta lejos del origen"
                    estatico = False
                    orig = timesmplace(connection, cursor, oraux, trip[i], trip[i + 1], estatico)
                    # SI FUE UN VERDADERO DESTINO
                    if orig:
                        # print "se completo viaje y nuevo origen"
                        oraux = []  # agregado por threading
                else:
                    pass
        else:
            # VERIFICA SI DEL ORIGEN AL DESTINO SE MOVIO UNA DISTANCIA VERDADERA
            # print "no avanzo se quedo en el mismo lugar"
            if vincenty(oraux[0][3:], trip[i][3:]).meters > rdistance:
                # print "esta lejos del origen"
                if i < (nran-2):
                    estatico = trip[i][2] == trip[i + 1][2] and trip[i + 1][2] == trip[i + 2][2]
                else:
                    estatico = False
                # print "estara estatoico :", estatico
                orig = timesmplace(connection, cursor, oraux, trip[i], trip[i + 1], estatico)
                if orig:
                    # print "se completo viaje y nuevo origen"
                    oraux = []  # agregado por threading
            else:
                # print "nuevo origen siempre estuvo estatico"
                orig = True
                oraux = []
    oraux = []


def genpooldb(poolc):
    connecn = poolc.getconn()
    connecn.set_isolation_level(0)
    return connecn


def detstate(theadlist):
    aux = True
    for x in theadlist:
        aux = x.is_alive() or aux
    return aux


def main():
    connection_list = []
    processlist = []
    # PROBAR PRIMERO CON LOS CORES QUE TIENE
    n = multiprocessing.cpu_count()
    # DIVIDE TODOS LOS USUARIOS DE LISTAS CASI HOMOGENEAS
    tids = idslist(n)
    print tids
    poolc = psycopg2.pool.ThreadedConnectionPool(1, n+1, host='localhost',
                                                 dbname='senegal', user='chris',
                                                 password='')

    for x in range(0, n):
        connection_list.append(genpooldb(poolc))
    for j in range(0, n):
        processlist.append(multithre(target=tripi, args=(connection_list[j],
                           connection_list[j].cursor(), tids[j])))

    for i in range(0, n):
        processlist[i].start()

    for h in xrange(0, n):
        processlist[h].join()

    # close connections
    if not(detstate(processlist)):
        poolc.closeall()
        closeconnection(conn, cur)

if __name__ == "__main__":
    main()
