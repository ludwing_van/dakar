from django.db import models
from django import forms
import psycopg2
from django.forms.extras.widgets import *
# Create your models here.

conn_string = "host='localhost' dbname='senegal' user='chris' password=''"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()


class Antenas(models.Model):
    """docstring for Antenas"""
    def __init__(self, consulta):
        self.consulta = consulta

    def getAntenas(self):
        cur.execute(self.consulta)
        listcoor = cur.fetchall()
        strcoord = []
        n = len(listcoor)
        for x in xrange(0, n):
            arraux = [listcoor[x][0], listcoor[x][1], listcoor[x][2]]
            strcoord.insert(x, arraux)
        return strcoord


class Destino(models.Model):
    def __init__(self, consulta):
        self.consulta = consulta

    def newdest(self):
        cur.execute(self.consulta)
        val = cur.fetchall()
        print(len(val))
        if (len(val) != 0):
            val = val[0]
            if(val[1]):
                val = val[1]
            else:
                val = 0
            return val
        else:
            return 0

    def latlond(self):
        cur.execute(self.consulta)
        # cur.execute("SELECT lat, lon from destino where\
        #             extract(day from fechahora) = 9\
        #             and\
        #             extract(hour from fechahora)\
        #             between 21 and 23\
        #             UNION ALL SELECT lat, lon\
        #             from destino where\
        #             (extract(day from fechahora) = 9\
        #             and\
        #             extract(hour from\
        #             fechahora) between 0 and 7)")
        listcoor = cur.fetchall()
        strcoord = []
        n = len(listcoor)
        for x in xrange(0, n):
            arraux = [listcoor[x][0], listcoor[x][1], listcoor[x][2], listcoor[x][3]]
            # strcoord.insert(x, [listcoor[x][0], listcoor[x][1]])
            strcoord.insert(x, arraux)
        return strcoord


class Origen(models.Model):

    def __init__(self, consulta):
        self.consulta = consulta

    def __unicode__(self):
        return self.origen

    def latlon(self):
        # cur.execute("SELECT lat,lon from origen where extract(day from fechahora) = 9 and extract(hour from fechahora) between 8 and 20")
        # cur.execute("SELECT lat,lon,uid from origen limit 15000;")
        # ids lista de ids unicos
        cur.execute(self.consulta)
        ids = cur.fetchall()
        strcoord = []
        n = len(ids)
        for x in xrange(0, n):
            strcoord.insert(x, [ids[x][0], ids[x][1]])
        return strcoord
