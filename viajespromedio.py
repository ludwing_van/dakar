import psycopg2
import geopy
import math
from geopy.distance import vincenty
conn_string = "host='localhost' dbname='senegal' user='chris' password=''"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()

'''
Boston
boston = (42.3541165, -71.0693514)
Latitude:42.359968
Longitude:-71.060093
newyork = (40.7791472, -73.9680804)
vincenty(boston, newyork)
'''


def calculadistancias():
    cur.execute("SELECT count(*) from distribuciont")
    aux = cur.fetchone()
    # print int(aux[0])+1
    # for x in range(1, int(aux['count'])+1):
    for x in range(1, int(aux[0])+1):
        cur.execute("SELECT lat1,lon1,lat2,lon2 from distribuciont where id = {}".format(x))
        aux = cur.fetchone()
        site1 = aux[:2]
        site2 = aux[2:]
        dd = vincenty(site1, site2).kilometers
        # print dd
        cur.execute("UPDATE distribuciont SET distancia = %s WHERE id = %s", (dd, x))
        conn.commit()


def main():
    calculadistancias()
    cur.close()
    conn.close()

if __name__ == "__main__":
    main()
