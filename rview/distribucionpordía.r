# Fete nationale (jour de l independance) '2013-04-04'  JUEVES
# Fete du travail 1er mai  
# '2013-07-09' ramadan


library(ggplot2)
# library(ggmap)
library(png)
# library(dplyr)
library(DBI)
library(RPostgreSQL)



getdata <- function() {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  consu <- paste("select extract(week from forigen) as semana,extract(dow from forigen) as dia,count(*) from matrizodaux group by 1,2 order by 1")
  df <- dbGetQuery(con, consu)
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  return(df)
}




dts1 <- getdata()

g <-ggplot(data=dts1, aes(semana,count,color=dia, group=dia), alpha = .50)+ geom_line() + geom_point() + ylab("# Viajes")+xlab("d??a")
g <-ggplot(data=dts2, aes(dts1.count) , alpha = .50) + geom_density() +scale_x_log10() + scale_y_log10()
plot(g)
# +
# scale_fill_discrete(name="Experimental\nCondition",
#                   breaks=c("destinos", "origenes"),
#                   labels=c("Destinos", "Origenes"))
# print(g)

dir.create(file.path("outputgraficas"), showWarnings = FALSE)
png(file=paste0("./outputgraficas/","semana",".png",sep=""),width = 1200, height = 1200, units="px")
print(g)
dev.off() 
