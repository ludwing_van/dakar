library(ggplot2)
library(png)
library(DBI)
library(RPostgreSQL)
library(fitdistrplus)
library(MASS)
# library(poweRlaw)
library(poweRlaw)
distribucion <- function(dtframe){
#   xlognormal <- fitdist(y, "lnorm")
#   lognormaldat = fitdistr(y, densfun = "log-normal")
#   m = displ$new(yy)
#   estimate_xmin(m)
  lognormalvia = conlnorm$new(dtframe$distancias)
  est = estimate_xmin(lognormalvia)
  lognormalvia$setXmin(est)
  
  powerlawdis = conpl$new(dtframe$distancias)
  est2 = estimate_xmin(powerlawdis)
  powerlawdis$setXmin(est2)
  
  
  
  
}
# axis(1, at = seq(0, 50, by = 10), las=1)
# delimitar valores a menos del dataset a 
# dff2 <- dff[dff$distancia > 10,]
# min mas chico
powerlawdis2 = conpl$new(dff2$distancia)
est3 = estimate_xmin(powerlawdis2)
powerlawdis2$setXmin(est3)

dff2 <- dff[dff$distacia > 10,]
dff2 <- dff[dff$distancia >= 10, ]


get.data1 <- function() {
  drv <- dbDriver("PostgreSQL")
  # con <- dbConnect(drv, dbname="senegal")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  # "select round,count(round) from (select round(distancia) from distribuciont limit 50) as A group by 1 order by round"
  # m <- ggplot(df, aes(x=round))
  # m+geom_density()+ylim(0,1)
  consu <- paste("select distancia from distribuciont")
  vect <- dbGetQuery(con, consu)
  df <- data.frame(vect)
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  return(df)
}
# scale_colour_gradientn("Horas\ninicio",colours=rev(rainbow(7)),breaks=c(2,4,6,8,10,12,14,16,18,20,22)) 
dff <- get.data1()
lognormalvia = conlnorm$new(dff$distancia)
est = estimate_xmin(lognormalvia)
lognormalvia$setXmin(est)

powerlawdis = conpl$new(dff$distancia)
est2 = estimate_xmin(powerlawdis)
powerlawdis$setXmin(est2)

g <- ggplot(lognormalvia) + geom_line() 
# distribucion(dff)
# 
# g<-ggplot(dff, aes(factor(x = hdest),y = cnt, fill=factor(hori))) + geom_bar(stat="identity") +xlab('Destinos por hora') +
#   ylab('Usuarios')+scale_fill_manual("Horas\ninicio",values = rev(colors2),  drop = FALSE)
# scale_fill_discrete("Horas\ninicio",h=c(200,490))
# m <- ggplot(dff, aes(x=distancia))+geom_density()+xlab('Viajes')+ylab('distribucion')+scale_y_log10()
# setwd('/Users/chris/Documents/tesistest/final1/rview/')
# dir.create(file.path("outputgraficas"), showWarnings = FALSE)
# png(file=paste0("./outputgraficas/","distribuciondistancia_viajes",".png",sep=""),width = 1200, height = 1200, units="px")
# print(m)
# dev.off() 


