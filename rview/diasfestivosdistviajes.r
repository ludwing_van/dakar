# Fete nationale (jour de l independance) '2013-04-04'  JUEVES
# Fete du travail 1er mai  
# '2013-07-09' ramadan


library(ggplot2)
# library(ggmap)
library(png)
# library(dplyr)
library(DBI)
library(RPostgreSQL)



get.data1 <- function() {
  drv <- dbDriver("PostgreSQL")
  # con <- dbConnect(drv, dbname="senegal")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  vector <- character()
  for(i in 0:23){
    consu <- paste("select count(uid) from (select * from matrizodaux where forigen::date = '2013-07-09') as A where extract(hour from  A.forigen) = ",i)
    # DIA independencia select count(uid) from (select * from matrizodaux where forigen::date = '2013-04-04') as A where extract(hour from  A.forigen) =
    #  DIA TRABAJO '2013-05-01'
    print(consu)
    vector[i+1] <- dbGetQuery(con, consu)
  }
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  df <- data.frame(unlist(vector),c(0:23))
  colnames(df) <- c("val","hour")
  return(df)
}
get.data2 <- function(typec) {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  vector <- character()
  for(i in 0:23){
    consu <- paste("select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-08') as A where extract(hour from  A.forigen) = ",i)
    # Inde "select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-10') as A where extract(hour from  A.forigen) = "
    # tra '2013-01-09'
    print(consu)
    vector[i+1] <- dbGetQuery(con, consu)
  }
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  df <- data.frame(unlist(vector),c(0:23))
  colnames(df) <- c("val","hour")
  return(df)
}

get.data3 <- function(typec) {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  vector <- character()
  for(i in 0:23){
    consu <- paste("select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-15') as A where extract(hour from  A.forigen) = ",i)
    # ("select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-17') as A where extract(hour from  A.forigen) = "
    # tra '2013-01-16
    print(consu)
    vector[i+1] <- dbGetQuery(con, consu)
  }
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  df <- data.frame(unlist(vector),c(0:23))
  colnames(df) <- c("val","hour")
  return(df)
}


get.data4 <- function(typec) {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  vector <- character()
  for(i in 0:23){
    consu <- paste("select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-22') as A where extract(hour from  A.forigen) = ",i)
    # "select count(uid) from (select * from matrizodaux where forigen::date = '2013-01-31') as A where extract(hour from  A.forigen) = "
    # trab '2013-01-30'
    print(consu)
    vector[i+1] <- dbGetQuery(con, consu)
  }
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  df <- data.frame(unlist(vector),c(0:23))
  colnames(df) <- c("val","hour")
  return(df)
}

get.data5 <- function(typec) {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  vector <- character()
  for(i in 0:23){
    consu <- paste("select count(uid) from (select * from matrizodaux where forigen::date = '2013-02-06') as A where extract(hour from  A.forigen) = ",i)
    # ("select count(uid) from (select * from matrizodaux where forigen::date = '2013-02-14') as A where extract(hour from  A.forigen) = "
    print(consu)
    vector[i+1] <- dbGetQuery(con, consu)
  }
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  df <- data.frame(unlist(vector),c(0:23))
  colnames(df) <- c("val","hour")
  return(df)
}




dts1 <- get.data1()
dts2 <- get.data2()
dts3 <- get.data3()
dts4 <- get.data4()
# dts5 <- get.data5()

g <-ggplot()+ geom_line(data=dts1, aes(hour, val),color="dodgerblue4",alpha = .50) + 
  geom_line(data=dts2, aes(hour, val),color="darkorange2",alpha = .50) +
  geom_line(data=dts3, aes(hour, val),color="darkorange2",alpha = .50) +
  geom_line(data=dts4, aes(hour, val),color="darkorange2",alpha = .50) +
  # geom_line(data=dts5, aes(hour, val),color="darkorange2",alpha = .50) +
  geom_point(data=dts1, aes(hour, val),color="dodgerblue4",alpha = .50, size=3) + 
  geom_point(data=dts2, aes(hour, val),color="darkorange2",alpha = .50,size=3) +
  geom_point(data=dts3, aes(hour, val),color="darkorange2",alpha = .50,size=3) +
  geom_point(data=dts4, aes(hour, val),color="darkorange2",alpha = .50,size=3) +
  # geom_point(data=dts5, aes(hour, val),color="darkorange2",alpha = .50,size=3) +
  ylab("# Viajes")+xlab("Hora")
# +
# scale_fill_discrete(name="Experimental\nCondition",
#                   breaks=c("destinos", "origenes"),
#                   labels=c("Destinos", "Origenes"))
# print(g)

dir.create(file.path("outputgraficas"), showWarnings = FALSE)
png(file=paste0("./outputgraficas/","ramadan",".png",sep=""),width = 1200, height = 1200, units="px")
print(g)
dev.off() 
