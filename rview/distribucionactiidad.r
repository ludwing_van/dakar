# cantidad de llamadas o actividad por mes y por d'ia aleatorio
Sys.setlocale("LC_CTYPE", 'es_ES.UTF-8')
Sys.setenv(LANG = "es_ES.UTF-8")
library(ggplot2)
# library(ggmap)
library(png)
# library(dplyr)
library(DBI)
library(RPostgreSQL)



getdata <- function() {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv, host='localhost', port='5432', dbname='senegal',user='chris', password='')
  consu <- paste("select A.count as llamadas, count(*) as cuenta from(
        select B.uid,count(*) from febrero
				as B group by 1 order by count) as A group by 1 order by cuenta DESC")
  # select B.uid,count(*) from (select * from enero union select * from febrero union select * from noviembre)
  # consu <- paste("select A.llamadas, count(*) from (
  #                 select uid, count(*) as llamadas from febrero where fechahora::date = '2013-02-14' group by 1 ) as A group by 1 order by 2 DESC")
  df <- dbGetQuery(con, consu)
  #cierra la conexion 
  dbDisconnect(con)
  #Libera recursos del driver
  dbUnloadDriver(drv)
  return(df)
}




dts1 <- getdata()
dts2 <- getdata()
# dts5 <- get.data5() geom_bar(stat="identity") 
colnames(dts1) <- c("llamadas","cuenta")
g <- ggplot(data=dts1[dts1$llamadas > 4  & dts1$llamadas < 100, ], aes(x=llamadas, y=cuenta), alpha = .50)+ geom_line() +
     ylab("N??mero de usuarios")+xlab("N??mero de llamadas")+
     scale_x_continuous(breaks = seq(0,500,10) )+ggtitle("Frecuencia de llamadas mensual promedio")+
    theme(plot.title = element_text(family = "Trebuchet MS", color="#666666", face="bold", size=28)) +
    theme(axis.title = element_text(family = "Trebuchet MS", color="#666666", face="bold", size=20)) 
  
plot(g)
# +
# scale_fill_discrete(name="Experimental\nCondition",
#                   breaks=c("destinos", "origenes"),
#                   labels=c("Destinos", "Origenes"))
# print(g)

dir.create(file.path("outputgraficas"), showWarnings = FALSE)
png(file=paste0("./outputgraficas/","llamadamensuales",".png",sep=""),width = 800, height = 800, units="px")
print(g)
dev.off() 

