import psycopg2
from consultas import *
import psycopg2.pool
import multiprocessing
import math
# mayor interaccion por usuario

conn_string = "host='localhost' dbname='senegal' user='chris' password=''"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()


class multipro(multiprocessing.Process):
    def __init__(self, target, args):
        self.target = target
        self.args = args
        multiprocessing.Process.__init__(self)

    def run(self):
        self.target(*self.args)


def detstate(theadlist):
    aux = True
    for x in theadlist:
        aux = x.is_alive() or aux
    return aux


def getUsers(n):
    print "entro"
    cur.execute("SELECT DISTINCT(uid) from matrizodaux")
    ids = cur.fetchall()
    # print ids, "\n"
    nu = len(ids)
    div = int(math.ceil(nu/n))
    tids = []
    val = nu % n
    # Si la lista esta dividida en partes desiugales
    for x in range(0, n):
        if val != 0 and x == (n-1):
            tids.append(ids[x*div:])
        else:
            tids.append(ids[x*div:(x+1)*div])
    if val != 0:
        aux = tids[n-1][:div]  # valores normalizados ultimo espacio
        aux2 = tids[n-1][div:]  # sobrantes
        tids[n-1] = aux
        for x in range(0, val):
            tids[x].append(aux2[x])
    return tids


def genpooldb(poolc):
    connecn = poolc.getconn()
    connecn.set_isolation_level(0)
    return connecn


def Sclose():
    cur.close()
    conn.close()


def listadepuntos(connection, cursor, lusers):
    for x in lusers:
        # print x
        # print lusers.index(x)
        # print len(lusers)
        print "Restantes: ", len(lusers)-lusers.index(x)
        cursor.execute(get_did(str(x[0])))
        did = cursor.fetchall()
        cursor.execute(get_oid(str(x[0])))
        oid = cursor.fetchall()
        # pdb.set_trace()
        if len(oid) != 0 and len(did) != 0:
            if did[0][0] != oid[0][0]:
                cursor.execute("INSERT INTO origenes_destinos_u(uid, residencia_comun, actividad_comun, ocurrencias_residencia, ocurrencias_actividad ) VALUES (%s, %s, %s, %s, %s)", (x[0], oid[0][0], did[0][0], oid[0][1], did[0][1]))
                connection.commit()


def main():
    n = multiprocessing.cpu_count()
    connection_list = []
    # theadlist = []
    processlist = []
    tids = getUsers(n)
    poolc = psycopg2.pool.ThreadedConnectionPool(1, n+1, host='localhost',
                                                 dbname='senegal',
                                                 user='chris',
                                                 password='')
    for x in range(0, n):
        connection_list.append(genpooldb(poolc))

    for j in range(0, n):
        processlist.append(multipro(target=listadepuntos, args=(connection_list[j],
                           connection_list[j].cursor(), tids[j])))
    # for i in range(0, n):
    #     theadlist[i].start()
    for i in range(0, n):
        processlist[i].start()

    for h in xrange(0, n):
        processlist[h].join()
    # close connections
    if not(detstate(processlist)):
        poolc.closeall()
        Sclose()


if __name__ == "__main__":
    main()
